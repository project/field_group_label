CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

Field Group Label allows the user to customize field group labels differently
for each node or entity.

This field won't be rendered on its own, just used to replace it's field group
label.

Also note that if the field has an empty value, the default label won't be
overridden.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/field_group_label

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/field_group_label


REQUIREMENTS
------------

This module requires the following outside of Drupal core:

 * Field Group - https://www.drupal.org/project/field_group


INSTALLATION
------------

Install the Field Group Label module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.


MAINTAINERS
-----------

 * CRZDEV - https://www.drupal.org/u/crzdev

Supporting organization

 * Metadrop - https://www.drupal.org/metadrop
