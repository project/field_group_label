<?php

namespace Drupal\field_group_label\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'field_group_label_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "field_group_label_formatter",
 *   label = @Translation("Field Group Label"),
 *   field_types = {
 *     "field_group_label_field_type"
 *   }
 * )
 */
class FieldGroupLabelFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $elements['#access'] = FALSE;
    $item = $items->first();

    if ($item instanceof FieldItemInterface) {
      $field_type = $this->pluginDefinition['field_types'] ?? [];
      $elements['#field_group_label'] = $this->viewValue($item);
      $elements['#field_type'] = current($field_type);
    }

    return $elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item): string {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    $text = Html::escape($item->getString());
    return nl2br($text);
  }

}
